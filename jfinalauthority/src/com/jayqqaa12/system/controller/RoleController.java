package com.jayqqaa12.system.controller;

import com.jayqqaa12.UrlConfig;
import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.shiro.ShiroDbRealm;
import com.jayqqaa12.system.model.Role;
import com.jayqqaa12.system.validator.RoleValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/role", viewPath = UrlConfig.SYSTEM)
public class RoleController extends Controller
{

	public void list()
	{
		renderJson(Role.dao.list());
	}

	public void tree()
	{

		int pid = getParaToInt("id", 0);
		renderJson(Role.dao.getTree(pid));

	}

	public void grant()
	{
		Role role = getModel(Role.class);
		String res_ids = getPara("res_ids");
		renderJsonResult(Role.dao.grant(role.getId(), res_ids));
		
	     ShiroCache.clearAuthorizationInfoAll();

	}

	@Override
	@Before(value = { RoleValidator.class })
	public void add()
	{
		renderJsonResult(getModel(Role.class).save());
	}

	@Override
	@Before(value = { RoleValidator.class })
	public void edit()
	{
		renderJsonResult(getModel(Role.class).update());
	}

	public void delete()
	{
		renderJsonResult(Role.dao.deleteById(getPara("id")));
	}

}
