package com.jayqqaa12.common;

import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.jbase.util.KindEditor;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/common/file")
public class FileController extends Controller
{
	
	
	public void upload()
	{
		renderJson(KindEditor.upload(this));
	}

	public void fileManage()
	{
		   renderJson(KindEditor.fileManage(getRequest()));
	}
	

	

}
